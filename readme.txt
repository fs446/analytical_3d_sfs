Frank Schultz
frank.schultz@uni-rostock.de

A Tutorial on Analytic Approaches for 3D Interior Sound Field Synthesis Using the Single Layer Potential
http://spatialaudio.net/comparing-approaches-to-the-spherical-and-planar-single-layer-potentials-for-interior-sound-field-synthesis/

Analytical_3D_SFS.git
https://username@bitbucket.org/fs446/analytical_3d_sfs.git

Updates:
1st PDF-Version 2014-06-26:
Schultz_A Tutorial on Analytic Approaches for 3D Interior Sound Field Synthesis Using the Single Layer Potential_2014_08_26.pdf